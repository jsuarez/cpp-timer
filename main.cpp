#include "timer.h"
#include <iostream>
#include <unistd.h>

int main(int argc, char *argv[])
{
	timer t;
	sleep(3);
	std::cout << "Elapsed time in millis " << t.elapsed() << " ms" << std::endl;
	std::cout << "Elapsed time in millis " << t.elapsed_micro() << " µs" << std::endl;
	std::cout << "Elapsed time in millis " << t.elapsed_nano() << " ns" << std::endl;
}
