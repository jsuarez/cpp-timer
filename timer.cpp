#include <chrono>
#include "timer.h"

timer::timer()
{
	start_time = std::chrono::steady_clock::now();
}

size_t timer::elapsed() const
{
	auto end = std::chrono::steady_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(end - start_time).count();
}

size_t timer::elapsed_micro() const
{
	auto end = std::chrono::steady_clock::now();
	return std::chrono::duration_cast<std::chrono::microseconds>(end - start_time).count();
}

size_t timer::elapsed_nano() const
{
	auto end = std::chrono::steady_clock::now();
	return std::chrono::duration_cast<std::chrono::nanoseconds>(end - start_time).count();
}

void timer::reset()
{
	start_time = std::chrono::steady_clock::now();
}
