#ifndef FILE_TIMER
#define FILE_TIMER

#include <chrono>

class timer  
{
	private:
		std::chrono::time_point<std::chrono::steady_clock> start_time;

	public:
		timer();
		size_t elapsed() const;
		size_t elapsed_micro() const;
		size_t elapsed_nano() const;
		void reset();
};

#endif /* !FILE_TIMER */
