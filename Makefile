CC=clang++
CFLAGS=-Wall -std=c++20 -O3

# DESCOMENTAR Y COMENTAR EL BLOQUE DE LA LIBTIMER PARA COMPILAR EN MODO DESARROLLO

# main: timer.o main.o
# 	$(CC) $(CFLAGS) -o main timer.o main.o

# main.o:
# 	$(CC) $(CFLAGS) -c -o main.o main.cpp
install: libtimer.a
	cp -v timer.h /usr/local/include/
	cp -v libtimer.a /usr/local/lib

libtimer.a: timer.o
	ar rcs libtimer.a timer.o

timer.o:
	$(CC) $(CFLAGS) -c -o timer.o timer.cpp

clean:
	rm -fr *.o main libtimer.a

